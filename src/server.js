import express from 'express';
// import session from 'express-session';
// import passport from 'passport';

class Server {
	constructor({
		config, router, logger, userAuthMiddleware, clientAuthMiddleware,
		errorMiddleware,
	}) {
		this.config = config;
		this.logger = logger;
		this.express = express();

		this.express.disable('x-powered-by');
		// this.express.use(session({
		// 	secret: 'node-restful-kajda',
		// 	resave: true,
		// 	saveUninitialized: false,
		// }));
		if (userAuthMiddleware) this.express.use(userAuthMiddleware.middleware);
		if (clientAuthMiddleware) this.express.use(clientAuthMiddleware.middleware);
		// this.express.use(passport.session());
		this.express.use(router);
		this.express.use(errorMiddleware);
	}

	start() {
		return new Promise((resolve) => {
			const http = this.express
				.listen(this.config.web.port, () => {
					const { port } = http.address();
					this.logger.info(`Running as process #${process.pid}, listening at port ${port}`);
					this.logger.info(`Environment: ${process.env.ENVIRONMENT}`);
					resolve();
				});
		});
	}
}

export default Server;

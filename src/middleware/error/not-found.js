import ResourceNotFound from '../../error/resource-not-found';

export default (req, res, next) => next(new ResourceNotFound());

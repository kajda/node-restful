import httpStatus from 'http-status';

export default ({ logger }) => (err, req, res, next) => {
	const status = err.status || httpStatus.INTERNAL_SERVER_ERROR;
	const errObj = {
		status,
		type: httpStatus[`${status}_NAME`],
		title: err.title || httpStatus[status],
		detail: err.detail || err.message || httpStatus[`${status}_MESSAGE`],
		instance: req.path,
	};
	if (err.errors) errObj.errors = err.errors;
	if (err.stack) errObj.stack = err.stack;
	logger.error(JSON.stringify(errObj));

	res.header('Content-Type', 'application/problem+json').status(status).json(errObj);
};

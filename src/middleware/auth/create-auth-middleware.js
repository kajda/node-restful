export default (passport, strategy, resultProp) => (req, res, next) => {
	passport.authenticate(strategy, (err, entity, info) => {
		if (err) next(err);
		if (info) next(info);
		req[resultProp] = entity;
		next();
	})(req, res, next);
};

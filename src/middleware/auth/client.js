import { Passport } from 'passport';
import LocalStrategy from 'passport-local';
import { Strategy as JWTStrategy, ExtractJwt } from 'passport-jwt';
import { UnauthorizedClient } from '../../error';
import createAuthMiddleware from './create-auth-middleware';

const clientPassport = new Passport();

export default ({ model, config }) => {
	const clientLocalAuthOpts = {
		usernameField: 'id',
		passwordField: 'secret',
	};

	const clientJwtOpts = {
		jwtFromRequest: ExtractJwt.fromHeader('api-token'),
		secretOrKey: config.api.accessControl.jwt.secret,
	};

	const ClientModel = model[config.api.accessControl.model.client || 'Client'];

	const clientJwtAuth = new JWTStrategy(clientJwtOpts, async (payload, done) => {
		try {
			const client = await ClientModel.findByPk(payload.id);

			if (!client) {
				return done(null, false);
			}

			const plainClient = {
				...client.toJSON(),
				token: client.generateToken(
					config.api.accessControl.jwt.secret,
					config.api.accessControl.jwt.expiresIn,
				),
			};

			return done(null, plainClient);
		} catch (e) {
			return done(e, false);
		}
	});

	const clientLocalAuth = new LocalStrategy(clientLocalAuthOpts, async (id, secret, done) => {
		try {
			const client = await ClientModel.findByPk(id);
			if (!client) {
				return done(null, false, new UnauthorizedClient('Client not registered.'));
			} if (!client.authenticate(secret)) {
				return done(null, false, new UnauthorizedClient('Incorrect client secret.'));
			}

			const plainClient = {
				...client.toJSON(),
				token: client.generateToken(
					config.api.accessControl.jwt.secret,
					config.api.accessControl.jwt.expiresIn,
				),
			};

			return done(null, plainClient);
		} catch (e) {
			return done(e, false);
		}
	});


	clientPassport.use('clientLocal', clientLocalAuth);
	clientPassport.use('clientJwt', clientJwtAuth);

	return {
		middleware: clientPassport.initialize({ userProperty: 'client' }),
		local: createAuthMiddleware(clientPassport, 'clientLocal', 'client'),
		jwt: createAuthMiddleware(clientPassport, 'clientJwt', 'client'),
	};
};

import { Passport } from 'passport';
import LocalStrategy from 'passport-local';
import AnonymousStrategy from 'passport-anonymous';
import { Strategy as JWTStrategy, ExtractJwt } from 'passport-jwt';
import { UnauthorizedLogin, InvalidUserToken } from '../../error';
import createAuthMiddleware from './create-auth-middleware';

const userPassport = new Passport();

export default ({ model, config }) => {
	const userLocalAuthOpts = {
		usernameField: 'email',
		passwordField: 'password',
	};

	const userJwtOpts = {
		jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
		secretOrKey: config.api.accessControl.jwt.secret,
	};

	const UserModel = model[config.api.accessControl.model.user || 'User'];

	const userAnonymous = new AnonymousStrategy();

	const userJwtAuth = new JWTStrategy(userJwtOpts, async (payload, done) => {
		try {
			const user = await UserModel.findByPk(payload.id);

			if (!user) {
				return done(null, false, new InvalidUserToken());
			}

			const plainUser = {
				...user.toJSON(),
				token: user.generateToken(
					config.api.accessControl.jwt.secret,
					config.api.accessControl.jwt.expiresIn,
				),
			};

			return done(null, plainUser);
		} catch (e) {
			return done(e, false);
		}
	});

	const userLocalAuth = new LocalStrategy(userLocalAuthOpts, async (email, password, done) => {
		try {
			const user = await UserModel.findByEmail(email);
			if (!user || !(await user.authenticate(password))) {
				return done(null, false, new UnauthorizedLogin());
			}

			const plainUser = {
				...user.toJSON(),
				token: user.generateToken(
					config.api.accessControl.jwt.secret,
					config.api.accessControl.jwt.expiresIn,
				),
			};

			return done(null, plainUser);
		} catch (e) {
			return done(e, false);
		}
	});


	userPassport.use('userLocal', userLocalAuth);
	userPassport.use('userJwt', userJwtAuth);
	userPassport.use('userAnonymous', userAnonymous);

	return {
		middleware: userPassport.initialize({ userProperty: 'user' }),
		jwt: createAuthMiddleware(userPassport, ['userJwt', 'userAnonymous'], 'user'),
		local: createAuthMiddleware(userPassport, 'userLocal', 'user'),
	};
};

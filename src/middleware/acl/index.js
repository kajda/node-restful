import { ResourceNotAllowed } from '../../error';

class AclMiddleware {
	constructor({ model, config, loggers }) {
		this.model = model[config.api.accessControl.model.role];
		this.apiBaseUrlRe = new RegExp(`^${config.api.baseUrl}/`);
		this.subResourceRe = new RegExp('(?<=\\w+)/\\d+');
		this.logger = loggers.getLogger('acl');
	}

	async getDefaultRoleId() {
		const role = await this.model.findOne({
			where: { default: 1 },
		});
		return role.id;
	}

	async isAllowed(user, roleId, resource, method, inherited = false) {
		const role = await this.model.findByPk(roleId);
		const permissions = JSON.parse(role.permissions);

		if (permissions[resource]
      && permissions[resource].allow
      && permissions[resource].allow.includes(method.toLowerCase())
		) {
			this.logger.info(`${resource}.${method}: Access granted for ${user ? user.last_name : 'anonymous'} as ${role.label}${inherited ? ' (inherited)' : ''}`);
			return true;
		}

		if (permissions[resource]
      && permissions[resource].deny
      && permissions[resource].deny.includes(method.toLowerCase())
		) {
			this.logger.warn(`${resource}.${method}: Access denied for ${user ? user.last_name : 'anonymous'} as ${role.label}${inherited ? ' (inherited)' : ''}`);
			return false;
		}

		if (role.parent) {
			return this.isAllowed(user, role.parent, resource, method, true);
		}

		this.logger.warn(`${resource}.${method}: Access denied (by default) for ${user ? user.last_name : 'anonymous'} as ${role.label}${inherited ? ' (inherited)' : ''}`);
		return false;
	}

	middleware() {
		return async (req, res, next) => {
			const {
				user, baseUrl, method,
			} = req;
			const resource = baseUrl.replace(this.apiBaseUrlRe, '');
			const role = await (user ? user.acl_role_id : this.getDefaultRoleId());
			const allowed = await this.isAllowed(user, role, resource, method);
			if (allowed) return next();
			return next(new ResourceNotAllowed(role));
		};
	}
}

export default AclMiddleware;

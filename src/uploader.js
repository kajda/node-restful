import multer from 'multer';

export default ({ config }) => multer({
	dest: config.api.uploadPath,
});

import { Router } from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import compression from 'compression';
import methodOverride from 'method-override';
import { ResourceNotFound } from './error';

export default ({
	containerMiddleware,
	config,
	userAuthMiddleware,
	clientAuthMiddleware,
	apiRoutes,
}) => {
	const rootRouter = Router();
	const apiRouter = Router();

	apiRouter
		.use('/access-token', [clientAuthMiddleware.jwt, userAuthMiddleware.local], (req, res) => res.send({ token: req.user.token }))
		.use('/whoami', [clientAuthMiddleware.jwt, userAuthMiddleware.jwt], (req, res) => res.send(req.user))
		.use('/', apiRoutes);

	rootRouter
		.use(containerMiddleware)
		.use(methodOverride('X-HTTP-Method-Override'))
		.use(cors(config.api.cors))
		.use(bodyParser.json())
		.use(compression())

		.post('/api-token', clientAuthMiddleware.local, (req, res) => res.send({ token: req.client.token }))
		.use('/api', apiRouter)

		.all('/*', (req, res, next) => next(new ResourceNotFound()));


	return rootRouter;
};

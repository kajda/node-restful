import Log4js from 'log4js';

export default {
	default: ({ config }) => {
		if (config.logging) Log4js.configure(config.logging);
		return Log4js.getLogger();
	},
	all: ({ config }) => {
		if (config.logging) Log4js.configure(config.logging);
		return Log4js;
	},
};

import { scopePerRequest } from 'awilix-express';
import app from '../application';
import server from '../server';
import logger from '../logger';
import userAuth from '../middleware/auth/user';
import clientAuth from '../middleware/auth/client';
import aclMiddleware from '../middleware/acl';
import errorMiddleware from '../middleware/error';
import router from '../router';
import uploader from '../uploader';

const {
	createContainer,
	asClass,
	asFunction,
	asValue,
} = require('awilix');

const container = createContainer();

container
	.register({
		containerMiddleware: asValue(scopePerRequest(container)),
	})
	.register({
		app: asClass(app).singleton(),
		server: asClass(server).singleton(),
		router: asFunction(router).singleton(),
		upload: asFunction(uploader).singleton(),
	})
	.register({
		logger: asFunction(logger.default).singleton(),
		loggers: asFunction(logger.all).singleton(),
	})

	// middlewares
	.register({
		userAuthMiddleware: asFunction(userAuth).singleton(),
		clientAuthMiddleware: asFunction(clientAuth).singleton(),
		aclMiddleware: asClass(aclMiddleware).singleton(),
		errorMiddleware: asFunction(errorMiddleware).singleton(),
	});

export default container;

/* eslint-disable class-methods-use-this */
import HttpStatus from 'http-status';
import { Router } from 'express';
import Sequelize from 'sequelize';
import { ResourceNotFound } from './error';
import validators from './validators';

const OPS_RE = /_(gt|lt|gte|lte)$/;
const RANGE_RE = /^\[(\d+)[-,](\d+)\]$/;

class CrudService {
	constructor(resourceName, model, validations) {
		this.resourceName = resourceName;
		this.model = model;
		this.validations = validations;
	}

	getRouter() {
		if (!this.$router) {
			const router = new Router();

			router.get('/', this.list.bind(this));
			router.get('/:id(\\d+)', this.retrive.bind(this));
			router.post('/', validators.validateSchema(this.validations.post), this.create.bind(this));
			router.put('/:id(\\d+)', validators.validateSchema(this.validations.put), this.update.bind(this));
			router.delete('/:id(\\d+)', this.destroy.bind(this));

			this.$router = router;
		}
		return this.$router;
	}

	getSortOptions(req) {
		const { sort } = req.query;
		if (!sort) return {};
		const [field, direction] = JSON.parse(sort);
		return {
			order: [
				[field, direction],
			],
		};
	}

	getRangeOptions(req) {
		const { range } = req.query;
		if (!range) return {};
		const pRange = range.match(RANGE_RE);
		if (!pRange) return {};
		const from = parseInt(pRange[1], 10);
		const to = parseInt(pRange[2], 10);
		if (from >= to) return {};
		return {
			limit: to - from,
			offset: from,
		};
	}

	getEmbedOptions(req) {
		const { embed } = req.query;
		if (!embed) return {};
		const pEmbed = JSON.parse(embed);
		return {
			include: pEmbed,
		};
	}

	getFilterOptions(req) {
		const { filter } = req.query;
		if (!filter) return {};
		const filterObj = JSON.parse(filter);
		return {
			where: Object.keys(filterObj).reduce((acc, fStr) => {
				if (fStr === 'q') return acc; // TODO fulltext
				const match = fStr.match(OPS_RE);
				let field = fStr;
				let op = Array.isArray(filterObj[fStr]) ? 'in' : 'eq';
				if (match && match[1]) {
					[, op] = match;
					field = fStr.replace(OPS_RE, '');
				}
				acc[field] = {
					[Sequelize.Op[op]]: filterObj[fStr],
				};
				return acc;
			}, {}),
		};
	}


	/**
   * Enables to add extra properties
   * to the object that is going to be inserted.
   */
	extendCreateObject(obj /* , req */) {
		return obj;
	}


	/**
   * Enables to specifies (options, most probably filter)
   * that applies to all find operations.
   */
	getFindOptions(req) {
		return {
			...this.getFilterOptions(req),
			...this.getRangeOptions(req),
			...this.getSortOptions(req),
			...this.getEmbedOptions(req),
		};
	}

	async retrieveResource(id, req) {
		const resource = await this.model.findByPk(id, this.getFindOptions(req));
		if (!resource) throw new ResourceNotFound();
		return resource;
	}

	async retrive(req, res, next) {
		try {
			const {
				id,
			} = req.params;
			const resource = await this.retrieveResource(id, req);
			res.status(HttpStatus.OK)
				.send(resource);
		} catch (e) {
			next(e);
		}
	}

	async create(req, res, next) {
		try {
			const result = await this.model.create(
				this.extendCreateObject(req.body, req),
			);
			res.status(HttpStatus.CREATED)
				.set({ Location: `/${this.resourceName}/${result.id}` })
				.send(result);
		} catch (e) {
			next(e);
		}
	}

	async update(req, res, next) {
		try {
			const {
				id,
			} = req.params;
			const resource = await this.retrieveResource(id, req);

			await resource.update({
				...resource,
				...req.body,
			});
			res.status(HttpStatus.OK)
				.send(resource);
		} catch (e) {
			next(e);
		}
	}

	async destroy(req, res, next) {
		try {
			const {
				id,
			} = req.params;
			const result = await this.retrieveResource(id, req);

			await result.destroy();
			res.status(HttpStatus.OK)
				.send(result);
		} catch (e) {
			next(e);
		}
	}

	async list(req, res, next) {
		try {
			const options = this.getFindOptions(req);
			const result = await this.model.findAndCountAll(options);
			const { offset = 0, limit = result.rows.length } = options;
			res.status(result.count > limit ? HttpStatus.PARTIAL_CONTENT : HttpStatus.OK)
				.set({ 'Content-Range': `${this.resourceName} ${offset}-${offset + limit}/${result.count}` })
				.send(result.rows);
		} catch (e) {
			next(e);
		}
	}
}

export default CrudService;

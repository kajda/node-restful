import HttpStatus from 'http-status';

export default class ResourceNotAllowed extends Error {
	constructor(role, message = HttpStatus['403_MESSAGE']) {
		super();
		this.type = HttpStatus['403_NAME'];
		this.message = message;
		this.title = `Request not allowed for role ${role}`;
		this.status = HttpStatus.FORBIDDEN;
		Error.captureStackTrace(this, ResourceNotAllowed);
	}
}

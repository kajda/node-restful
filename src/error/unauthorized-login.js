import HttpStatus from 'http-status';

export default class UnauthorizedLogin extends Error {
	constructor(message = 'Incorrect email or password.') {
		super();
		this.type = HttpStatus['401_NAME'];
		this.message = message;
		this.title = 'Unauthorized login';
		this.status = HttpStatus.UNAUTHORIZED;
		Error.captureStackTrace(this, UnauthorizedLogin);
	}
}

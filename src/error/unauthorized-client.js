import HttpStatus from 'http-status';

export default class UnauthorizedClient extends Error {
	constructor(message = HttpStatus['401_MESSAGE']) {
		super();
		this.type = HttpStatus['401_NAME'];
		this.message = message;
		this.title = 'Unauthorized client';
		this.status = HttpStatus.UNAUTHORIZED;
		Error.captureStackTrace(this, UnauthorizedClient);
	}
}

import HttpStatus from 'http-status';

export default class InvalidUserToken extends Error {
	constructor(message = 'Token passed within Authorization header is not valid.') {
		super();
		this.type = HttpStatus['400_NAME'];
		this.message = message;
		this.title = 'Unauthorized user';
		this.status = HttpStatus.BAD_REQUEST;
		Error.captureStackTrace(this, InvalidUserToken);
	}
}

import HttpStatus from 'http-status';

export default class ResourceNotFound extends Error {
	constructor(message = HttpStatus['404_MESSAGE']) {
		super();
		this.type = HttpStatus['404_NAME'];
		this.message = message;
		this.title = 'Resource not found.';
		this.status = HttpStatus.NOT_FOUND;
		Error.captureStackTrace(this, ResourceNotFound);
	}
}

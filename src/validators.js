import { Validator } from 'jsonschema';
import httpStatus from 'http-status';

const validator = new Validator();

export default {

	validateSchema: (rules) => (req, res, next) => {
		const result = validator.validate(req.body, rules);
		if (!result.valid) {
			next({
				status: httpStatus.BAD_REQUEST,
				title: 'Invalid Request Payload',
				detail: result.toString(),
				errors: result.errors,
			});
		}
		next();
	},

	validateFileUpload: () => (req, res, next) => {
		next();
	},

};

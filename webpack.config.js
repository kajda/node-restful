const path = require('path');
const { peerDependencies, dependencies } = require('./package.json');

const config = {
	mode: process.env.NODE_ENV,
	entry: {
		lib: `${__dirname}/src/index.js`,
	},
	devtool: 'source-map',
	output: {
		path: __dirname,
		filename: '[name]/index.js',
		library: ' node-restful',
		libraryTarget: 'umd',
	},
	module: {
		rules: [
			{
				test: /(\.js)$/,
				exclude: /(node_modules)/,
				use: { loader: 'babel-loader' },
			},
			{
				test: /(\.js)$/,
				exclude: /(node_modules)/,
				use: { loader: 'eslint-loader' },
			},
		],
	},
	optimization: {
		nodeEnv: false,
	},
	target: 'node',
	resolve: {
		extensions: ['.js'],
		alias: {
			errors: path.resolve(__dirname, './src/error'),
		},
	},
	node: {
		fs: 'empty',
		net: 'empty',
	},
	externals: [
		...Object.keys(dependencies),
		...Object.keys(peerDependencies),
	],
};

module.exports = config;
